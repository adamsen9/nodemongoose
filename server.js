var express = require("express")
var http = require("http")
var path = require("path")
var app = express()
var fs = require("fs")
var bodyParser = require("body-parser")

const mongoose = require("mongoose")
mongoose.connect("mongodb://localhost:27017/nodetest1")

var db = mongoose.connection
db.on("error", console.error.bind(console, "connection error:"))
db.once("open", function() {
    console.log("connected to db!")
})

app.use(bodyParser.json())

// dynamically include routes (Controller)
fs.readdirSync("./controllers").forEach(function(file) {
    if (file.substr(-3) == ".js") {
        route = require("./controllers/" + file)
        route.controller(app)
    }
})

var server_port = 8080
var server_ip_address = "0.0.0.0"

app.listen(server_port, server_ip_address, () => {
    console.log("Server started!")
})
