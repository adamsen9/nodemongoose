var Meme = require("../models/meme")

module.exports.controller = function(app) {
    //get all
    app.get("/meme/getMemes", function(req, res) {
        Meme.find((err, memes) => {
            if (err) {
                return res.status(500).send(err)
            } else {
                return res.status(200).send(memes)
            }
        })
    })

    //get specific meme
    app.get("/meme/getMeme", function(req, res) {
        Meme.findById(req.headers.memeid, (err, meme) => {
            if (err) {
                return res.status(500).send(err)
            } else {
                return res.status(200).send(meme)
            }
        })
    })

    app.post("/meme/createMeme", function(req, res) {
        //TODO: validate the request body
        var newMeme = new Meme(req.body)

        newMeme.save(err => {
            if (err) {
                return res.status(500).send(err)
            } else {
                return res.status(201).send(newMeme)
            }
        })
    })

    app.put("/meme/updateMeme", function(req, res) {
        //TODO: validate the request body
        Meme.findByIdAndUpdate(
            req.headers.memeid,
            req.body,
            { new: true },
            (err, meme) => {
                if (err) {
                    const response = {
                        message:
                            "No meme with that id found; nothing has been updated",
                        id: req.headers.memeid
                    }
                    return res.status(404).send(response)
                } else {
                    return res.status(200).send(meme)
                }
            }
        )
    })

    app.delete("/meme/deleteMeme", function(req, res) {
        Meme.findByIdAndRemove(req.body.memeId, (err, Meme) => {
            if (err) {
                return res.status(500).send(err)
            } else {
                if (Meme == null) {
                    const response = {
                        message:
                            "No meme with that id found; Nothing has been deleted from the database.",
                        id: req.body.memeId
                    }
                    return res.status(404).send(response)
                } else {
                    const response = {
                        message: "Meme successfully deleted",
                        id: Meme._id
                    }
                    return res.status(200).send(response)
                }
            }
        })
    })
}
