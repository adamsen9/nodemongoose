const mongoose = require("mongoose")
const Schema = mongoose.Schema

const memeSchema = new Schema({
    title: String
})

module.exports = mongoose.model("meme", memeSchema)